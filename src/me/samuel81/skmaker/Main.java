package me.samuel81.skmaker;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

public class Main {

	private static Map<String, Template> templates = new HashMap<>();

	public static void main(String[] args)
			throws URISyntaxException, EncryptedDocumentException, InvalidFormatException, IOException {

		inittemplateIndividu();
		String filePath = args[0];
		String skName = args[1];
		File folder = new File(filePath);
		File sk = new File(skName);
		if (folder.exists()) {
			DataProcess processor = new DataProcess(folder, sk);
			processor.process();
		} else {
			System.out.println("Folder not exist");
		}

	}

	/**
	 * Load excel templateIndividu from local file to memory
	 * 
	 * @throws URISyntaxException
	 */
	private static void inittemplateIndividu() throws URISyntaxException {
		File mainFolder = new File(
				Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().toString().replace("maker.jar", "").replace("file:/", "") + "Template");
		for(File jenis : mainFolder.listFiles()) {
			if(jenis.isDirectory()) {
				String name = jenis.getName();
				for(File type : jenis.listFiles()) {
					if(type.isDirectory()) {
						String tname = type.getName();
						String combine = name+"-"+tname;
						templates.put(combine.toUpperCase(), new Template(type));
					}
				}
			}
		}
	}
	
	/**
	 * Get template based on the code
	 * @param jenis
	 * @param type
	 * @return
	 */
	public static Template getTemplateFile(String jenis, String type) {
		return templates.get(new StringBuilder().append(jenis).append("-").append(type).toString().toUpperCase());
	}
	
	public static File getLampiran(String jenis, String sktype) {
		return getTemplateFile(jenis, sktype).getLampiran();
	}
	
	public static File getSK(String jenis, String sktype, String type) {
		if(type.equalsIgnoreCase("HGB"))
			return getTemplateFile(jenis, sktype).getHGB();
		if(type.equalsIgnoreCase("HM"))
			return getTemplateFile(jenis, sktype).getHM();
		if(type.equalsIgnoreCase("HP"))
			return getTemplateFile(jenis, sktype).getHP();
		return null;
	}
	
	public static File getLampiranIndividu(String jenis) {
		return getLampiran(jenis, "INDIVIDU");
	}
	
	public static File getSKIndividu(String jenis, String type) {
		return getSK(jenis, "INDIVIDU", type);
	}
	
	public static File getLampiranCs(String jenis) {
		return getLampiran(jenis, "CS");
	}
	
	public static File getSKCs(String jenis, String type) {
		return getSK(jenis, "CS", type);
	}
	
	public static File getLampiranBundle(String jenis) {
		return getLampiran(jenis, "BUNDLE");
	}
	
	public static File getSKBundle(String jenis, String type) {
		return getSK(jenis, "BUNDLE", type);
	}

}
