package me.samuel81.skmaker;

import java.io.File;

public class Template {
	
	File lampiran, hgb, hm, hp;
	
	public Template(File folder) {
		lampiran = new File(folder, "LAMPIRAN.docx");
		hgb = new File(folder, "HGB.docx");
		hm = new File(folder, "HM.docx");
		hp = new File(folder, "HP.docx");
	}
	
	public File getLampiran() {
		return lampiran;
	}
	
	public File getHGB() {
		return hgb;
	}
	
	public File getHM() {
		return hm;
	}
	
	public File getHP() {
		return hp;
	}
	
}
