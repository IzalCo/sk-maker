package me.samuel81.skmaker;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;

public class SK {

	private String jenis, noSk, tglSk, rtSk, rwSk, kelSk, kecSk, noBerkas, di310, namaKtp, nik, alamatKtp, rtKtp, rwKtp,
			kelKtp, kecKtp, kotaKtp, provKtp, nib, noSu, tglSu, luasTanah, tglPph, team, riwayat;

	int pos = 0;

	private File file;
	private File lampiran;

	private List<SK> children = new ArrayList<>();

	public SK() {

	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getNoSk() {
		return noSk;
	}

	public void setNoSk(String noSk) {
		this.noSk = noSk;
	}

	public String getTglSk() {
		return tglSk;
	}

	public void setTglSk(String tglSk) {
		this.tglSk = tglSk;
	}

	public String getRtSk() {
		return rtSk;
	}

	public void setRtSk(String rtSk) {
		this.rtSk = rtSk;
	}

	public String getRwSk() {
		return rwSk;
	}

	public void setRwSk(String rwSk) {
		this.rwSk = rwSk;
	}

	public String getKelSk() {
		return kelSk;
	}

	public void setKelSk(String kelSk) {
		this.kelSk = kelSk;
	}

	public String getKecSk() {
		return kecSk;
	}

	public void setKecSk(String kecSk) {
		this.kecSk = kecSk;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getDi310() {
		return di310;
	}

	public void setDi310(String di310) {
		this.di310 = di310;
	}

	public String getNamaKtp() {
		return namaKtp;
	}

	public void setNamaKtp(String namaKtp) {
		this.namaKtp = namaKtp;
	}

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getAlamatKtp() {
		return alamatKtp;
	}

	public void setAlamatKtp(String alamatKtp) {
		this.alamatKtp = alamatKtp;
	}

	public String getRtKtp() {
		return rtKtp;
	}

	public void setRtKtp(String rtKtp) {
		this.rtKtp = rtKtp;
	}

	public String getRwKtp() {
		return rwKtp;
	}

	public void setRwKtp(String rwKtp) {
		this.rwKtp = rwKtp;
	}

	public String getKelKtp() {
		return kelKtp;
	}

	public void setKelKtp(String kelKtp) {
		this.kelKtp = kelKtp;
	}

	public String getKecKtp() {
		return kecKtp;
	}

	public void setKecKtp(String kecKtp) {
		this.kecKtp = kecKtp;
	}

	public String getNib() {
		return nib;
	}

	public void setNib(String nib) {
		this.nib = nib;
	}

	public String getNoSu() {
		return noSu;
	}

	public void setNoSu(String noSu) {
		this.noSu = noSu;
	}

	public String getTglSu() {
		return tglSu;
	}

	public void setTglSu(String tglSu) {
		this.tglSu = tglSu;
	}

	public String getLuasTanah() {
		return luasTanah;
	}

	public void setLuasTanah(String luasTanah) {
		this.luasTanah = luasTanah;
	}

	public String getTglPph() {
		return tglPph;
	}

	public void setTglPph(String tglPph) {
		this.tglPph = tglPph;
	}

	public String getTglLahir() {
		String tglLahir = nik.substring(6, 13);
		int tgl = Integer.parseInt(tglLahir.substring(0, 2)) > 40 ? Integer.parseInt(tglLahir.substring(0, 2)) - 40
				: Integer.parseInt(tglLahir.substring(0, 2));
		int bln = Integer.parseInt(tglLahir.substring(2, 4));
		int thn = Integer.parseInt(tglLahir.substring(4, 6));
		return tgl + "-" + bln + "-" + (thn < 20 ? "20" + thn : "19" + thn);
	}

	public String getTglLahir(String nik) {
		String tglLahir = nik.substring(6, 13);
		int tgl = Integer.parseInt(tglLahir.substring(0, 2)) > 40 ? Integer.parseInt(tglLahir.substring(0, 2)) - 40
				: Integer.parseInt(tglLahir.substring(0, 2));
		int bln = Integer.parseInt(tglLahir.substring(2, 4));
		int thn = Integer.parseInt(tglLahir.substring(4, 6));
		return tgl + "-" + bln + "-" + (thn < 20 ? "20" + thn : "19" + thn);
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public File getFolder() {
		return file;
	}

	public String getKotaKtp() {
		return kotaKtp;
	}

	public void setKotaKtp(String kotaKtp) {
		this.kotaKtp = kotaKtp;
	}

	public String getProvKtp() {
		return provKtp;
	}

	public void setProvKtp(String provKtp) {
		this.provKtp = provKtp;
	}

	public String getRiwayat() {
		return riwayat;
	}

	public void setRiwayat(String riwayat) {
		this.riwayat = riwayat;
	}

	public String getTemplateCode(String tanggal) {
		if (Util.before(tanggal, "26-08-2019") || tanggal.equalsIgnoreCase("26-08-2019")) {
			return "3";
		} else if (Util.before(tanggal, "14-10-2019") || tanggal.equalsIgnoreCase("14-10-2019")) {
			return "1";
		} else if (Util.before(tanggal, "22-12-2019") || tanggal.equalsIgnoreCase("22-12-2019")) {
			return "4";
		} else if (Util.before(tanggal, "31-12-2019") || tanggal.equalsIgnoreCase("31-12-2019")) {
			return "2";
		}
		return "2";
	}

	public void build(File folder) throws Exception {

		DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

		file = new File(folder + "/TEAM " + team + "/" + kelSk.toUpperCase() + "/" + jenis + "/"
				+ (namaKtp + " - RT." + rtSk + " RW." + rwSk + " (1 BERKAS)"));
		if (!file.exists()) {
			file.mkdirs();
		} else {
			for (File f : file.listFiles())
				f.delete();
		}

		File lampiran = new File(file, "LAMPIRAN.doc");
		FileUtils.copyFile(Main.getLampiranIndividu(getTemplateCode(this.tglSk)), lampiran);
		LinkedHashMap<String, String> fill = new LinkedHashMap<>();
		fill.put("JENIS_HAK", this.jenis);
		fill.put("NO_SK", this.noSk);
		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(this.tglSk)));
		fill.put("NAMA", this.namaKtp);
		fill.put("TGL_LAHIR", this.getTglLahir());
		fill.put("NIK", this.nik);
		fill.put("ALAMAT_KTP", Util.capitalizer(this.alamatKtp));
		fill.put("RT_KTP", this.rtKtp);
		fill.put("RW_KTP", this.rwKtp);
		fill.put("KEL_KTP", Util.capitalizer(this.kelKtp));
		fill.put("KEC_KTP", Util.capitalizer(this.kecKtp));
		fill.put("TGL_PPH", this.tglPph);
		fill.put("NO_BERKAS", this.noBerkas);
		fill.put("TGL_SU", this.tglSu);
		fill.put("TAHUN_SU", this.tglSu.substring(6));
		fill.put("NO_SU", this.noSu);
		fill.put("KELURAHAN", Util.capitalizer(this.kelSk));
		fill.put("NIB", this.nib);
		fill.put("LUAS", this.luasTanah);
		fill.put("RT", this.rtSk);
		fill.put("RW", this.rwSk);
		fill.put("KECAMATAN", Util.capitalizer(this.kecSk));
		fill.put("TIM", "Tim " + team);

		fill.put("KOTA_KTP",
				Util.capitalizer(this.kotaKtp.contains("KOTA") ? this.kotaKtp : "Kabupaten " + this.kotaKtp));
		fill.put("PROVINSI_KTP", Util.capitalizer(this.provKtp));

		List<String> riws = new ArrayList<>();
		for (String str : Util.capitalizer(this.riwayat).replaceAll("(\r\n|\n\r|\r|\n)", "").split(";")) {
			riws.add(Util.capitalizer(str) + ";");
		}

		riws.add("Surat Pernyataan"
				+ (this.tglPph.substring(8).equalsIgnoreCase("18") ? " Penguasaan / Pemilikan Tanah" : "") + " Tanggal "
				+ this.tglPph + ";");
		riws.add("Surat Pernyataan BPHTB Terhutang Tanggal " + this.tglPph + ";");
		riws.add("Surat Pernyataan PPH Terhutang Tanggal " + this.tglPph + ";");

		Collections.sort(riws, new Comparator<String>() {
			@Override
			public int compare(String v1, String v2) {
				SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
				try {
					Date d1 = sdformat.parse(v1.substring(v1.lastIndexOf(";") - 11));
					Date d2 = sdformat.parse(v2.substring(v2.lastIndexOf(";") - 11));
					return d1.compareTo(d2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		String str = riws.stream().map(s -> String.valueOf(s)).collect(Collectors.joining("\r\n"));
		fill.put("RIWAYAT", this.riwayat.equalsIgnoreCase("") ? "" : str);

		// fill.put("2018", this.tglPph.substring(8).equalsIgnoreCase("18") ? "
		// Penguasaan / Pemilikan Tanah" : "");
		fill.put("TAHUN", this.tglSk.substring(6));

		Util.fillForm(lampiran, fill);

		fill.clear();

		File sk = new File(file, "SK.doc");

		FileUtils.copyFile(Main.getSKIndividu(getTemplateCode(this.tglSk), jenis), sk);

		fill.put("NO_SK", this.noSk);
		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(this.tglSk)));
		fill.put("TIM", "Tim " + team);
		fill.put("DI310", this.di310);
		fill.put("TAHUN_310",
				(Integer.parseInt(di310) <= 100 && tglSk.substring(8).equalsIgnoreCase("20")) ? "2020" : "2019");
		fill.put("NAMA", this.namaKtp);
		fill.put("KELURAHAN", this.kelSk);
		fill.put("KECAMATAN", this.kecSk);
		fill.put("TAHUN", this.tglSk.substring(6));

		Util.fillForm(sk, fill);
	}

	public void buildBanyak(File folder) throws Exception {

		DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

		String[] nama = namaKtp.replace("\n", "").split(";");
		String[] niks = nik.replace("\n", "").split(";");

		file = new File(folder + "/TEAM " + team + "/" + kelSk.toUpperCase() + "/" + jenis + "/"
				+ (nama[0] + " cs - RT." + rtSk + " RW." + rwSk + " (1 BERKAS)"));
		if (!file.exists()) {
			file.mkdirs();
		} else {
			for (File f : file.listFiles())
				f.delete();
		}

		File lampiran = new File(file, "LAMPIRAN.doc");
		FileUtils.copyFile(Main.getLampiranCs(getTemplateCode(this.tglSk)), lampiran);
		LinkedHashMap<String, String> fill = new LinkedHashMap<>();
		fill.put("JENIS_HAK", this.jenis);
		fill.put("NO_SK", this.noSk);
		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(this.tglSk)));

		String str = "";
		for (int i = 0; i < nama.length; i++) {
			str += "Nama : " + nama[i] + "\r\nTanggal lahir : " + getTglLahir(niks[i]) + "\r\nNIK : " + niks[i]
					+ ";\r\n\r\n";
		}

		fill.put("IDENT", str);
		if (!alamatKtp.equalsIgnoreCase("-")) {
			String alamat = "Alamat : " + Util.capitalizer(alamatKtp) + "\r\nRT." + rtKtp + " RW." + rwKtp + "\r\nKel. " + Util.capitalizer(this.kelKtp)
					+ "\r\nKec. " + Util.capitalizer(this.kecKtp) + "\r\n" + Util.capitalizer(this.kotaKtp)
					+ " Provinsi " + Util.capitalizer(this.provKtp) + ";\r\n\r\n";
			fill.put("ALAMAT", alamat);
		} else {
			fill.put("ALAMAT", "Alamat: Yang bertempat tinggal sesuai Kartu Tanda Penduduk masing-masing;\r\n\r\n");
		}
		fill.put("TAHUN", this.tglSk.substring(6));
		fill.put("TGL_PPH", this.tglPph);
		fill.put("NO_BERKAS", this.noBerkas);
		fill.put("TGL_SU", this.tglSu);
		fill.put("TAHUN_SU", this.tglSu.substring(6));
		fill.put("NO_SU", this.noSu);
		fill.put("KELURAHAN", Util.capitalizer(this.kelSk));
		fill.put("NIB", this.nib);
		fill.put("LUAS", this.luasTanah);
		fill.put("RT", this.rtSk);
		fill.put("RW", this.rwSk);
		fill.put("KECAMATAN", Util.capitalizer(this.kecSk));
		fill.put("TIM", "Tim " + team);
		// fill.put("DI310", this.di310);
		// fill.put("TAHUN_310", (Integer.parseInt(di310) <= 100 &&
		// tglSk.substring(8).equalsIgnoreCase("20")) ? "2020" : "2019");
		fill.put("KOTA_KTP",
				Util.capitalizer(this.kotaKtp.contains("KOTA") ? this.kotaKtp : "Kabupaten " + this.kotaKtp));
		fill.put("PROVINSI_KTP", Util.capitalizer(this.provKtp));

		List<String> riws = new ArrayList<>();
		for (String x : Util.capitalizer(this.riwayat).replaceAll("(\r\n|\n\r|\r|\n)", "").split(";")) {
			riws.add(Util.capitalizer(x) + ";");
		}

		riws.add("Surat Pernyataan"
				+ (this.tglPph.substring(8).equalsIgnoreCase("18") ? " Penguasaan / Pemilikan Tanah" : "") + " Tanggal "
				+ tglPph + ";");
		riws.add("Surat Pernyataan BPHTB Terhutang Tanggal " + tglPph + ";");
		riws.add("Surat Pernyataan PPH Terhutang Tanggal " + tglPph + ";");

		Collections.sort(riws, new Comparator<String>() {
			@Override
			public int compare(String v1, String v2) {
				SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
				try {
					Date d1 = sdformat.parse(v1.substring(v1.lastIndexOf(";") - 11));
					Date d2 = sdformat.parse(v2.substring(v2.lastIndexOf(";") - 11));
					return d1.compareTo(d2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		String x = riws.stream().map(s -> String.valueOf(s)).collect(Collectors.joining("\r\n"));

		fill.put("RIWAYAT", this.riwayat.equalsIgnoreCase("") ? "" : x);
		// fill.put("2018", this.tglPph.substring(8).equalsIgnoreCase("18") ? "
		// Penguasaan / Pemilikan Tanah" : "");

		Util.fillForm(lampiran, fill);

		fill.clear();

		File sk = new File(file, "SK.doc");

		FileUtils.copyFile(Main.getSKCs(getTemplateCode(this.tglSk), jenis), sk);

		fill.put("NO_SK", this.noSk);
		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(this.tglSk)));
		fill.put("TAHUN", this.tglSk.substring(6));
		fill.put("TIM", "Tim " + team);
		fill.put("DI310", this.di310);
		fill.put("TAHUN_310",
				(Integer.parseInt(di310) <= 100 && tglSk.substring(8).equalsIgnoreCase("20")) ? "2020" : "2019");
		String names = "";
		for (int i = 0; i < nama.length; i++) {
			names += nama[i];
			if (i < nama.length - 1)
				names += ", ";
		}
		fill.put("NAMA-S", names.split(", ")[0] + ", Cs");
		fill.put("JML", String.valueOf(nama.length));
		fill.put("NAMA", names);
		fill.put("KELURAHAN", this.kelSk);
		fill.put("KECAMATAN", this.kecSk);

		Util.fillForm(sk, fill);
	}

	public void setAsParent(File folder) throws Exception {
		DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

		String[] nama = namaKtp.replace("\n", "").split(";");
		String[] niks = nik.replace("\n", "").split(";");

		file = new File(folder + "/TEAM " + team + "/" + kelSk.toUpperCase() + "/" + jenis + "/"
				+ (nama[0] + " - RT." + rtSk + " RW." + rwSk + " (${JML} BERKAS)"));
		if (!file.exists()) {
			file.mkdirs();
		} else {
			for (File f : file.listFiles())
				f.delete();
		}

		lampiran = new File(file, "LAMPIRAN.doc");
		FileUtils.copyFile(Main.getLampiranBundle(getTemplateCode(this.tglSk)), lampiran);
		LinkedHashMap<String, String> fill = new LinkedHashMap<>();

		fill.put("NO", String.valueOf(++pos));
		fill.put("TAHUN", this.tglSk.substring(6));
		fill.put("JENIS_HAK", this.jenis);

		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(this.tglSk)));

		String str = "";
		for (int i = 0; i < nama.length; i++) {
			str += "Nama : " + nama[i] + "\r\nTanggal lahir : " + getTglLahir(niks[i]) + "\r\nNIK : " + niks[i]
					+ ";\r\n\r\n";
		}

		fill.put("IDENT", str);
		if (!alamatKtp.equalsIgnoreCase("-")) {
			String alamat = "Alamat : " + Util.capitalizer(alamatKtp) + "\r\nRT." + rtKtp + " RW." + rwKtp + "\r\nKel. " + Util.capitalizer(this.kelKtp)
					+ "\r\nKec. " + Util.capitalizer(this.kecKtp) + "\r\n" + Util.capitalizer(this.kotaKtp)
					+ " Provinsi " + Util.capitalizer(this.provKtp) + ";\r\n\r\n";
			fill.put("ALAMAT", alamat);
		} else {
			fill.put("ALAMAT", "Alamat: Yang bertempat tinggal sesuai Kartu Tanda Penduduk masing-masing;\r\n\r\n");
		}

		fill.put("TGL_PPH", this.tglPph);
		fill.put("NO_BERKAS", this.noBerkas);
		fill.put("TGL_SU", this.tglSu);
		fill.put("TAHUN_SU", this.tglSu.substring(6));
		fill.put("NO_SU", this.noSu);
		fill.put("KELURAHAN", Util.capitalizer(this.kelSk));
		fill.put("NIB", this.nib);
		fill.put("LUAS", this.luasTanah);
		fill.put("RT", this.rtSk);
		fill.put("RW", this.rwSk);
		fill.put("KECAMATAN", Util.capitalizer(this.kecSk));
		fill.put("TIM", "Tim " + team);
		/*
		 * fill.put("DI310", this.di310); fill.put("TAHUN_310", (Integer.parseInt(di310)
		 * <= 100 && tglSk.substring(8).equalsIgnoreCase("20")) ? "2020" : "2019");
		 */
		fill.put("KOTA_KTP",
				Util.capitalizer(this.kotaKtp.contains("KOTA") ? this.kotaKtp : "Kabupaten " + this.kotaKtp));
		fill.put("PROVINSI_KTP", Util.capitalizer(this.provKtp));

		List<String> riws = new ArrayList<>();
		for (String x : Util.capitalizer(this.riwayat).replaceAll("(\r\n|\n\r|\r|\n)", "").split(";")) {
			riws.add(Util.capitalizer(x) + ";");
		}

		riws.add("Surat Pernyataan"
				+ (this.tglPph.substring(8).equalsIgnoreCase("18") ? " Penguasaan / Pemilikan Tanah" : "") + " Tanggal "
				+ tglPph + ";");
		riws.add("Surat Pernyataan BPHTB Terhutang Tanggal " + tglPph + ";");
		riws.add("Surat Pernyataan PPH Terhutang Tanggal " + tglPph + ";");

		Collections.sort(riws, new Comparator<String>() {
			@Override
			public int compare(String v1, String v2) {
				SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
				try {
					Date d1 = sdformat.parse(v1.substring(v1.lastIndexOf(";") - 11));
					Date d2 = sdformat.parse(v2.substring(v2.lastIndexOf(";") - 11));
					return d1.compareTo(d2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		String x = riws.stream().map(s -> String.valueOf(s)).collect(Collectors.joining("\r\n"));
		fill.put("RIWAYAT", this.riwayat.equalsIgnoreCase("") ? "" : x);
		fill.put("TAHUN", this.tglSk.substring(6));

		Util.fillForm(lampiran, fill);

		fill.clear();

	}

	public void addChildren(SK sk) throws Exception {
		children.add(sk);

		XWPFDocument tem = new XWPFDocument(
				OPCPackage.open(FileUtils.openInputStream(Main.getLampiranBundle(getTemplateCode(this.tglSk)))));
		XWPFTable template = tem.getTables().get(0);
		XWPFDocument document = new XWPFDocument(OPCPackage.open(FileUtils.openInputStream(lampiran)));
		for (XWPFTable tbl : document.getTables()) {
			tbl.addRow(template.getRow(2));
			break;
		}
		document.write(FileUtils.openOutputStream(lampiran));
		tem.close();
		document.close();
		DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

		String[] nama = sk.namaKtp.replace("\n", "").split(";");
		String[] niks = sk.nik.replace("\n", "").split(";");

		LinkedHashMap<String, String> fill = new LinkedHashMap<>();
		fill.put("NO", String.valueOf(++pos));
		fill.put("JENIS_HAK", jenis);

		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(tglSk)));
		fill.put("TAHUN", this.tglSk.substring(6));
		String str = "";
		for (int i = 0; i < nama.length; i++) {
			str += "Nama : " + nama[i] + "\r\nTanggal lahir : " + getTglLahir(niks[i]) + "\r\nNIK : " + niks[i]
					+ ";\r\n\r\n";
		}

		fill.put("IDENT", str);
		if (!sk.alamatKtp.equalsIgnoreCase("-")) {
			String alamat = "Alamat : " + Util.capitalizer(sk.alamatKtp) + "\r\nRT." + sk.rtKtp + " RW." + sk.rwKtp + "\r\nKel. "
					+ Util.capitalizer(sk.kelKtp) + "\r\nKec. " + Util.capitalizer(sk.kecKtp) + "\r\n"
					+ Util.capitalizer(sk.kotaKtp) + " Provinsi " + Util.capitalizer(sk.provKtp) + ";\r\n\r\n";
			fill.put("ALAMAT", alamat);

		} else {
			fill.put("ALAMAT", "Alamat: Yang bertempat tinggal sesuai Kartu Tanda Penduduk masing-masing;\r\n\r\n");
		}

		fill.put("TGL_PPH", sk.tglPph);
		fill.put("NO_BERKAS", sk.noBerkas);
		fill.put("TGL_SU", sk.tglSu);
		fill.put("TAHUN_SU", sk.tglSu.substring(6));
		fill.put("NO_SU", sk.noSu);
		fill.put("KELURAHAN", Util.capitalizer(kelSk));
		fill.put("NIB", nib.substring(0, 9) + sk.nib);
		fill.put("LUAS", sk.luasTanah);
		fill.put("RT", (sk.rtSk != null && !sk.rtSk.equalsIgnoreCase("")) ? sk.rtSk : rtSk);
		fill.put("RW", (sk.rwSk != null && !sk.rwSk.equalsIgnoreCase("")) ? sk.rwKtp : rwSk);
		fill.put("KECAMATAN", Util.capitalizer(kecSk));
		fill.put("TIM", "Tim " + team);
		/*
		 * fill.put("DI310", di310); fill.put("TAHUN_310", (Integer.parseInt(di310) <=
		 * 100 && tglSk.substring(8).equalsIgnoreCase("20")) ? "2020" : "2019");
		 */
		fill.put("KOTA_KTP", Util.capitalizer(sk.kotaKtp.contains("KOTA") ? sk.kotaKtp : "Kabupaten " + sk.kotaKtp));
		fill.put("PROVINSI_KTP", Util.capitalizer(sk.provKtp));

		List<String> riws = new ArrayList<>();
		for (String x : Util.capitalizer(sk.riwayat).replaceAll("(\r\n|\n\r|\r|\n)", "").split(";")) {
			riws.add(Util.capitalizer(x) + ";");
		}

		riws.add("Surat Pernyataan"
				+ (sk.tglPph.substring(8).equalsIgnoreCase("18") ? " Penguasaan / Pemilikan Tanah" : "") + " Tanggal "
				+ sk.tglPph + ";");
		riws.add("Surat Pernyataan BPHTB Terhutang Tanggal " + sk.tglPph + ";");
		riws.add("Surat Pernyataan PPH Terhutang Tanggal " + sk.tglPph + ";");

		Collections.sort(riws, new Comparator<String>() {
			@Override
			public int compare(String v1, String v2) {
				SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
				try {
					Date d1 = sdformat.parse(v1.substring(v1.lastIndexOf(";") - 11));
					Date d2 = sdformat.parse(v2.substring(v2.lastIndexOf(";") - 11));
					return d1.compareTo(d2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		String x = riws.stream().map(s -> String.valueOf(s)).collect(Collectors.joining("\r\n"));
		fill.put("RIWAYAT", sk.riwayat.equalsIgnoreCase("") ? "" : x);
		// fill.put("2018", sk.tglPph.substring(8).equalsIgnoreCase("18") ? " Penguasaan
		// / Pemilikan Tanah" : "");
		fill.put("TAHUN", this.tglSk.substring(6));
		Util.fillForm(lampiran, fill);

		fill.clear();
	}

	public void buildAnak(File folder) throws Exception {
		DateFormat formatter = DateFormat.getDateInstance(DateFormat.LONG, new Locale("id", "ID"));
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));

		LinkedHashMap<String, String> fill = new LinkedHashMap<>();

		File sk = new File(file, "SK.doc");

		FileUtils.copyFile(Main.getSKBundle(getTemplateCode(this.tglSk), jenis), sk);

		List<Integer> nos = new ArrayList<>();
		nos.add(Integer.parseInt(noSk));

		Set<String> names = new HashSet<>();
		if (namaKtp.contains(";")) {
			String n = "";
			String[] nama = namaKtp.replace("\n", "").split(";");
			for (int i = 0; i < nama.length; i++) {
				n += nama[i];
				if (i < nama.length - 1)
					n += ", ";
			}
			names.add(n);
		} else {
			names.add(namaKtp);
		}
		for (SK ch : children) {
			String nch = ch.namaKtp;
			nos.add(Integer.parseInt(ch.getNoSk()));
			if (nch.contains(";")) {
				String n = "";
				String[] nama = nch.replace("\n", "").split(";");
				for (int i = 0; i < nama.length; i++) {
					n += nama[i];
					if (i < nama.length - 1)
						n += ", ";
				}
				names.add(n);
			} else {
				names.add(nch);
			}
		}

		Collections.sort(nos);
		String noLengkapSK = "";
		int latest = -1;
		int similar = 0;
		for (int x = 0; x < nos.size(); x++) {
			int i = nos.get(x);
			if (latest == -1) {
				latest = i;
				noLengkapSK += i;
			} else if (i - latest > 1) {
				if (similar > 0) {
					noLengkapSK += "-" + latest;
					latest = -1;
					similar = 0;
					noLengkapSK += ", " + i;
					if (x < nos.size() - 1)
						noLengkapSK += ", ";
				} else if (similar == 0) {
					latest = -1;
					noLengkapSK += ", " + i;
				}
			} else if (i - latest == 1) {
				similar++;
				latest = i;
			}
		}
		if (similar > 0)
			noLengkapSK += "-" + latest;
		// noLengkapSK = noLengkapSK.substring(0 ,noLengkapSK.lastIndexOf(","));
		fill.put("TAHUN", this.tglSk.substring(6));
		fill.put("NO_SK", noLengkapSK);
		fill.put("TGL_SK", formatter.format(new SimpleDateFormat("dd-MM-yyyy").parse(this.tglSk)));
		fill.put("TIM", "Tim " + team);
		fill.put("DI310", this.di310);
		fill.put("TAHUN_310",
				(Integer.parseInt(di310) <= 100 && tglSk.substring(8).equalsIgnoreCase("20")) ? "2020" : "2019");
		String nameFull = "";
		String[] nnn = new String[names.size()];
		names.toArray(nnn);
		for (int i = 0; i < nnn.length; i++) {
			nameFull += "" + (i + 1) + ". " + nnn[i];
			if (i < names.size() - 1)
				nameFull += ", ";
		}
		fill.put("JML", String.valueOf(names.size()));
		fill.put("FORMS", String.valueOf(children.size() + 1));
		fill.put("NAMA", nameFull);
		fill.put("NAMA-S", nameFull.split(", ")[0].replace("1. ", "") + ", Dkk");
		fill.put("KELURAHAN", this.kelSk);
		fill.put("KECAMATAN", this.kecSk);

		Util.fillForm(sk, fill);

		fill.clear();
		fill.put("NO_SK", noLengkapSK);
		Util.fillForm(lampiran, fill);

		File dfolder = new File(file.getAbsolutePath().replace("${JML}", String.valueOf(children.size() + 1)));
		if (dfolder.exists())
			dfolder.delete();
		file.renameTo(dfolder);
	}

}
