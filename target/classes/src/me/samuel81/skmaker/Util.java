package me.samuel81.skmaker;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TimeZone;

import pl.jsolve.templ4docx.core.Docx;
import pl.jsolve.templ4docx.core.VariablePattern;
import pl.jsolve.templ4docx.variable.BulletListVariable;
import pl.jsolve.templ4docx.variable.TextVariable;
import pl.jsolve.templ4docx.variable.Variables;

public class Util {

	private static DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

	public static String getNow() {
		format.setTimeZone(TimeZone.getDefault());
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return format.format(cal.getTime());
	}

	public static String parseTanggal(String str) {
		String tgl = null, bln = null, thn = null;
		if (str.length() == 6) {
			tgl = str.substring(0, 2);
			bln = str.substring(2, 4);
			thn = str.substring(4, 6);
		} else if (str.length() == 5) {
			tgl = "0" + str.substring(0, 1);
			bln = str.substring(1, 3);
			thn = str.substring(3, 5);
		} else {
			return str;
		}
		return tgl + "-" + bln + "-" + (Integer.parseInt(thn) <= 20 ? "20" + thn : "19" + thn);
	}

	public static boolean isExceed(String cal) {
		Date now = new Date();
		Date date = null;
		try {
			date = (Date) format.parse(cal);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date.before(now);
	}

	public static boolean before(String cal1, String cal2) {
		Date date = null, date1 = null;
		;
		try {
			date = (Date) format.parse(cal1);
			date1 = (Date) format.parse(cal2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date.before(date1);
	}

	/**
	 * @return string that has been capitalized the first letter
	 */
	private static String capitalize(String input) {
		return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
	}

	/**
	 * Creating an first letter uppercase
	 * 
	 * @param input
	 * @return
	 */
	private static String lowerFirst(String input) {
		String output = "";
		String[] test = input.contains(" ") ? input.split(" ") : null;
		if (test != null) {
			for (String s : test) {
				output = output == "" ? new StringBuilder().append(s.startsWith("(") ? s : capitalize(s)).toString()
						: new StringBuilder().append(output).append(" ").append(s.startsWith("(") ? s : capitalize(s))
								.toString();
			}
		}
		return test != null ? output : capitalize(input);
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static String capitalizer(String input) {
		return lowerFirst(input);
	}

	public static void fillForm(File form, LinkedHashMap<String, String> vars) throws Exception {
		
		Docx docx = new Docx(form.getPath());
		docx.setVariablePattern(new VariablePattern("${", "}"));

		// preparing variables
		Variables variables = new Variables();
		vars.forEach((k, v) -> {
			if (v.contains("\n")) {
				List<TextVariable> list = new ArrayList<TextVariable>();
				for (String par : v.split("\n")) {
					list.add(new TextVariable("${" + k + "}", par.substring(0, 1).toUpperCase() + par.substring(1)));
				}
				variables.addBulletListVariable(new BulletListVariable("${" + k + "}", list));
			} else {
				variables.addTextVariable(new TextVariable("${" + k + "}", v));
			}
		});

		// fill template
		docx.fillTemplate(variables);

		docx.save(form.getPath());
	}

}
