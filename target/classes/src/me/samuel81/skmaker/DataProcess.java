package me.samuel81.skmaker;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class DataProcess {

	private File excelDB;
	private File btTarget;

	public DataProcess(File btTarget, File skFile) {
		this.btTarget = btTarget;
		this.excelDB = skFile;
	}

	public int getTeam(String kecamatan) {
		if (kecamatan.equalsIgnoreCase("Cilincing")) {
			return 1;
		} else if (kecamatan.equalsIgnoreCase("Penjaringan")) {
			return 2;
		} else if (kecamatan.equalsIgnoreCase("Tanjung Priok")) {
			return 3;
		} else if (kecamatan.equalsIgnoreCase("Kelapa Gading")) {
			return 4;
		} else if (kecamatan.equalsIgnoreCase("Pademangan")) {
			return 4;
		} else if (kecamatan.equalsIgnoreCase("Koja")) {
			return 5;
		}
		return 5;
	}

	private String intToRoman(int num) {

		String m[] = { "", "M", "MM", "MMM" };
		String c[] = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
		String x[] = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
		String i[] = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };

		String thousands = m[num / 1000];
		String hundereds = c[(num % 1000) / 100];
		String tens = x[(num % 100) / 10];
		String ones = i[num % 10];

		String ans = thousands + hundereds + tens + ones;

		return ans;
	}

	/**
	 * Process all data in the folder also make an excel output from processed data
	 * 
	 * @throws IOException
	 */
	public void process() throws IOException, EncryptedDocumentException, InvalidFormatException {
		try {
			Workbook wb = WorkbookFactory.create(excelDB);
			Sheet sheet = wb.getSheetAt(0);
			Iterator<Row> rows = sheet.iterator();
			boolean first = true;

			SK currentParent = null;

			while (rows.hasNext()) {
				Row row = rows.next();
				row.forEach(c -> c.setCellType(CellType.STRING));
				if (first) {
					first = false;
					continue;
				}
				if (row.getCell(0) == null)
					continue;
				SK sk = new SK();
				boolean child = row.getCell(25) != null && row.getCell(25).getStringCellValue().equalsIgnoreCase("-");
				sk.setJenis(row.getCell(0).getStringCellValue());

				sk.setNoBerkas(row.getCell(1).getStringCellValue());

				if (!child) {
					sk.setRtSk(String.format("%03d", Integer.valueOf(row.getCell(2).getStringCellValue())));
					sk.setRwSk(String.format("%03d", Integer.valueOf(row.getCell(3).getStringCellValue())));
					sk.setKelSk(row.getCell(4).getStringCellValue());
					sk.setKecSk(row.getCell(5).getStringCellValue());
					sk.setDi310(row.getCell(6).getStringCellValue());
					sk.setTglSk(Util.parseTanggal(row.getCell(8).getStringCellValue()));
					sk.setNib(row.getCell(14).getStringCellValue());
					sk.setTeam(intToRoman(getTeam(sk.getKecSk())));
				} else {
					sk.setNib(row.getCell(9).getStringCellValue());
				}

				sk.setNoSk(row.getCell(7).getStringCellValue());

				sk.setNoSu(String.format("%05d", Integer.valueOf(row.getCell(10).getStringCellValue())));
				sk.setLuasTanah(row.getCell(11).getStringCellValue());
				sk.setTglSu(Util.parseTanggal(row.getCell(12).getStringCellValue()));

				sk.setTglPph(Util.parseTanggal(row.getCell(13).getStringCellValue()));

				String nama = row.getCell(15).getStringCellValue();
				boolean individu = !nama.contains(";");

				sk.setNamaKtp(nama);
				sk.setNik(row.getCell(16).getStringCellValue());
				sk.setAlamatKtp(row.getCell(17).getStringCellValue());
				String rt = row.getCell(18).getStringCellValue(), rw = row.getCell(19).getStringCellValue();
				sk.setRtKtp(rt.equalsIgnoreCase("-") ? rt : String.format("%03d", Integer.valueOf(rt)));
				sk.setRwKtp(rw.equalsIgnoreCase("-") ? rw : String.format("%03d", Integer.valueOf(rw)));
				sk.setKelKtp(row.getCell(20).getStringCellValue().replace("  ", " "));
				sk.setKecKtp(row.getCell(21).getStringCellValue().replace("  ", " "));
				sk.setKotaKtp(row.getCell(22).getStringCellValue().replace("  ", " "));
				sk.setProvKtp(row.getCell(23).getStringCellValue().replace("  ", " "));

				sk.setRiwayat(row.getCell(24).getStringCellValue().replace("  ", " "));

				if (row.getCell(25) != null && !row.getCell(25).getStringCellValue().equalsIgnoreCase("")) {
					String par = row.getCell(25).getStringCellValue();
					if (par.equalsIgnoreCase("YES")) {
						if (currentParent != null) {
							currentParent.buildAnak(btTarget);
							currentParent = null;
						}
						sk.setAsParent(btTarget);
						currentParent = sk;

					} else {
						currentParent.addChildren(sk);
					}
					continue;
				} else {
					if (currentParent != null) {
						currentParent.buildAnak(btTarget);
						currentParent = null;
					}
				}

				if (individu)
					sk.build(btTarget);
				else
					sk.buildBanyak(btTarget);
			}
			if (currentParent != null) {
				currentParent.buildAnak(btTarget);
			}
			wb.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
